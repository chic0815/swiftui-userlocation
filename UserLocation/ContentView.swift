//
//  ContentView.swift
//  UserLocation
//
//  Created by Jaesung on 2019/11/17.
//  Copyright © 2019 Jaesung. All rights reserved.
//

import SwiftUI
import MapKit

struct ContentView: View {
    
    var locationManager = CLLocationManager()
    
    var body: some View {
        ZStack {
            MapView(locationManager: locationManager).edgesIgnoringSafeArea(.all)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
